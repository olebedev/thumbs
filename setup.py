from setuptools import setup, find_packages

setup(
    name='django-thumbs',
    version='1',
    description='The easiest way to create thumbnails for your images with Django. Works with any StorageBackend.',
    author='Antonio, Oleg Lebedev',
    author_email='mail@olebedev.ru',
    url='https://bitbucket.org/olebedev/django-thumbs',
    packages=find_packages(),
    include_package_data=False,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ]
)
